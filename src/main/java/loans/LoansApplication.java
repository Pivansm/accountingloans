package loans;

import loans.domain.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@SpringBootApplication
public class LoansApplication {

    @Autowired
    private UserRepository userRepository;
    
    //private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public static void main(String[] args) {
        SpringApplication.run(LoansApplication.class, args);
    }

    @Bean
    CommandLineRunner runner() {
        return args -> {
            /*userRepository.save(new User("admin", bCryptPasswordEncoder.encode("admin"), "ADMIN"));
            userRepository.save(new User("user", bCryptPasswordEncoder.encode("user"), "USER"));*/
        };
    }
}
