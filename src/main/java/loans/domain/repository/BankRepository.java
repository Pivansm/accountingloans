package loans.domain.repository;

import loans.domain.entity.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BankRepository extends JpaRepository<Bank, Long> {
    //
    List<Bank> findByNameBank(String nameBank);
}
