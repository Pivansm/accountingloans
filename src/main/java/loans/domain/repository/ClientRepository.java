package loans.domain.repository;

import loans.domain.entity.Bank;
import loans.domain.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findByFullName(String fullName);

    @Query("select c from Client c where c.bank.id = :idd")
    Iterable<Client> findByBankRef(@Param("idd") Long id);

    @Query("select c from Bank c ")
    Iterable<Bank> findByBankAll();

}
