package loans.domain.repository;

import loans.domain.entity.CreditOffer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRepository extends CrudRepository<CreditOffer, Long> {
}
