package loans.domain.repository;

import loans.domain.entity.Bank;
import loans.domain.entity.Client;
import loans.domain.entity.Credit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CreditRepository extends CrudRepository<Credit, Long> {

    //@Query("select c from Credit c left join Bank t on t.id = c.bank_ref where t is null")
    //Optional<Credit> findCreditWithoutBank();

    @Query("select c from Credit c where c.bank.id = :idd")
    Iterable<Credit> findByBankRef(@Param("idd") Long id);

    @Query("select c from Bank c ")
    Iterable<Bank> findByBankAll();


}
