package loans.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class PaymentSchedule {

    @Id
    private Long id;
    private LocalDate paymentDate;
    private int countDay;

    private Double paymentAmount;
    private Double arrayMoney;
    private Double persent;
    private Double principalBalance;

}
