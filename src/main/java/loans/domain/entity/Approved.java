package loans.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
public class Approved implements Serializable {

    private static final long serialVersionUID = -1L;
    @Id
    private Long id;
    private String message;
    private boolean completed;
    private LocalDateTime created;
    private Long offerId;
}
