package loans.kafka.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import loans.domain.entity.CreditOffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class OfferProducer {
    private final static Logger log = LoggerFactory.getLogger(OfferProducer.class);

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    /**
     * Producer для CreditOffer
     * @param creditOffer
     * @param key
     */
    public void produceOffer(CreditOffer creditOffer, String key) {
        log.info("Producing the offer:=" + creditOffer);
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            //ow.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            kafkaTemplate.send("offer", key, ow.writeValueAsString(creditOffer));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.error("err:=", e.getMessage());
        }
    }
}
