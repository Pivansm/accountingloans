package loans.kafka.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;

@Slf4j
@Service
public class ApprovedConsumer {

    @KafkaListener(topics = "approved", groupId = "offer_id")
    public void msgListener(Object msg){
        log.info("=====Consuming the approved:=" + msg);
    }
}
