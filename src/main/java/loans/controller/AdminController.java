package loans.controller;

import loans.domain.repository.UserRepository;
import loans.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
public class AdminController {
    //@Autowired
    //private UserService userService;
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/admin")
    public String userList(Model model) {
        log.info("List user=================================");
        model.addAttribute("users", userRepository.findAll());
        return "admin/listUser";
    }

    //@PostMapping("/admin")
    //public String  deleteUser(@RequestParam(required = true, defaultValue = "" ) Long userId,
    //                          @RequestParam(required = true, defaultValue = "" ) String action,
    //                          Model model) {
    //    if (action.equals("delete")){
    //        userService.deleteUser(userId);
    //    }
    //    return "redirect:/admin";
    //}

    //@GetMapping("/admin/gt/{userId}")
    //public String  gtUser(@PathVariable("userId") Long userId, Model model) {
    //    model.addAttribute("allUsers", userService.usergtList(userId));
    //    return "admin";
    //}
}
