package loans.controller;

import loans.domain.entity.Bank;
import loans.domain.entity.Credit;
import loans.domain.repository.CreditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Контроллер для кредитов
 * @author Ivan
 *
 */
@Controller
public class CreditController {
    private final static Logger LOGGER = LoggerFactory.getLogger(CreditController.class);
    private List<String> typeCred;
    private final CreditRepository creditRepository;

    public CreditController(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
        typeCred = new ArrayList<>();
        typeCred.add("ANNUITY");
        typeCred.add("DIFFERENTIAL");
    }

    @GetMapping("credits.html")
    public String all(Model model) {
        LOGGER.info("credits");
        Iterable<Credit> iterable = creditRepository.findAll();
        model.addAttribute("credits", creditRepository.findAll());
        return "credits/listCredit";
    }

    @GetMapping(value = "/credits.html", params = "id")
    private String getCreditId(Long id, Model model) {
        System.out.println("id:=" + id);
        Optional<Credit> cr = creditRepository.findById(id);
        Iterable<Bank> banks =  creditRepository.findByBankAll();
        System.out.println("credit:=" + cr);
        creditRepository.findById(id)
                .ifPresent(credit -> model.addAttribute("credit", credit));
        model.addAttribute("allTypes", typeCred);
        model.addAttribute("banks", banks);
        return "credits/detailsCredit";
    }

    @GetMapping(value = "bankCredits", params = "id")
    public String allBankId(Long id, Model model) {
        LOGGER.info("======credits + bank:=" + id);
        model.addAttribute("credits", creditRepository.findByBankRef(id));
        return "credits/listCredit";
    }


    @PostMapping(value="/credits.html")
    public String addCredit(@ModelAttribute Credit newCredit, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            LOGGER.error("BINDING RESULT ERROR:=" + bindingResult.toString());
            return "/credits.html";
        } else {
            model.addAttribute("credit", newCredit);
            if (newCredit.getLimitCredit() != null) {
                creditRepository.save(newCredit);
                System.out.println("new credit added: " + newCredit);
            }
            return "redirect:/credits.html";
        }
    }

    @PostMapping("credit")
    public String processCredit(Credit cri, Model model) {
        LOGGER.info("Find client:=" + cri);
        Optional<Credit> credit = Optional.empty();
        model.addAttribute("credit", credit);
        model.addAttribute("allTypes", typeCred);
        return "credits/detailsCredit";
    }

    @PostMapping(value = "deleteCredit", params = "id")
    public String deleteCredit(Long id) {
        LOGGER.info("Delete credit id:=" + id);
        Optional<Credit> cr = creditRepository.findById(id);
        System.out.println("credit:=" + cr);
        creditRepository.deleteById(id);
        return "redirect:/credits.html";
    }


}
