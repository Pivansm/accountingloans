package loans.setting;

import loans.domain.repository.BankRepository;
//import loans.service.impl.UserDetailsServiceImpl;
import loans.toexcel.ExcelDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Autowired
    private BankRepository bankRepository;

    @Bean
    public ExcelDocument excelDocument() {
        return new ExcelDocument();
    }

}
