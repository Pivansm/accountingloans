package loans.setting;

import loans.domain.entity.User;
import loans.domain.repository.UserRepository;
//import loans.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    //Для тестирования
    /*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .passwordEncoder(passwordEncoder())
                .withUser("tester")
                .password(passwordEncoder().encode("secret"))
                .roles("ADMIN", "USER");
    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                //Доступ только для не зарегистрированных пользователей
                .antMatchers("/css/**", "/registration").not().fullyAuthenticated()
                //Для админов
                .antMatchers("/admin/**").hasRole("ADMIN")
                //для User
                .antMatchers("/clients/**", "/offers/**").hasRole("USER")
                //Доступ разрешен всем пользователям
                .antMatchers("/css/**", "/registration").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    //для теста
    /*@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                //Отключаем csrf
                .csrf()
                .disable()
                //все запросы к любой конечной точке должны быть авторизованы
                .authorizeRequests()
                //Доступ только для не зарегистрированных пользователей
                .antMatchers("/css/**", "/register/**").not().fullyAuthenticated()
                //Доступ только для пользователей с ролью Администратор
                .antMatchers("/admin/**").hasRole("ADMIN")
                //.antMatchers("/news").hasRole("USER")
                //Доступ разрешен всем пользователям
                .antMatchers("/resources/templates/**", "/css/**").permitAll()
                //Все остальные страницы требуют аутентификации
                .anyRequest().authenticated()
                .and()
                //Настройка для входа в систему
                .formLogin()
                .loginPage("/login")
                //Перенарпавление на главную страницу после успешного входа
                //.defaultSuccessUrl("/")
                //.permitAll()
                .and().httpBasic()
                //Отключаем, не храним информацию о сеансе пользователя
                .and().sessionManagement().disable()
                //logout
                .logout().permitAll()
                .logoutSuccessUrl("/login");
                //.logoutSuccessUrl("/")
                ;
    }*/

}
